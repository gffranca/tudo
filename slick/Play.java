package slick;

import java.util.logging.Level;
import java.util.logging.Logger;
import objetos.*;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import objetos.ListadeAster;
import java.awt.Font;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import slick.Game;
import static slick.Game.play;

public class Play extends BasicGameState{
    
    Nave nave;
    private long timer;
    public ListadeAster asters;
    public ListadeVida vidas;
    private static long delta_time;
    private long StartTime;
    private boolean reinicia = false;

    public Play(int state){
        
        
    }
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
        
        this.asters = new ListadeAster();
        this.nave = new Nave(gc);
        this.asters.novo_aster(gc);
        this.vidas = new ListadeVida(gc);
        this.StartTime = System.currentTimeMillis();
        asters.pontuacao = 0;
        
    }
    
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{
        
        g.setColor(Color.green);
        String str = String.valueOf(asters.pontuacao);
        g.drawString(str, gc.getWidth()/2 - 20,  10);
        this.nave.tiros.render(gc, g);
        this.nave.render(gc, g);
        this.asters.render(gc, g);
        this.vidas.render(gc, g);
    }
    
    public void update(GameContainer gc, StateBasedGame sbg, int i)throws SlickException{
        
        this.timer += i;
        if(this.timer >= 1200){
            
            this.asters.novo_aster(gc);
            this.timer = 0;
        }
        this.nave.update(gc, i);
        this.nave.tiros.update();
        this.asters.update(gc);
        this.calcula_delta_time();
        this.vidas.update(gc);
        
        reinicia = Nave.getPerdeu();
        if(reinicia == true){
            reinicia = false;
            sbg.getState(0).init(gc, sbg);
            sbg.enterState(0, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
            
        }
    }
    
    public int getID(){
        
        return 1;
    }
    
    private void calcula_delta_time(){
        
        long now_time = System.currentTimeMillis();
        this.delta_time = now_time - StartTime;
        this.StartTime = now_time;
        if (delta_time == 0 ) this.delta_time +=1;
    }
    public static float get_delta_time(){
        
        return delta_time;
    }
    public static float get_delta_time_seconds(){
        
        return (float) (delta_time*0.1)/1000;
    }
    
}
