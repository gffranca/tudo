package slick;

import objetos.Botao;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import static slick.Game.play;


public abstract class Menu extends BasicGameState{
   
    private Botao botao_jogar;
    
    public Menu(int state){}
    
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
        
       this.botao_jogar = new Botao(70,70,"Jogar");
    }
    
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{
        
        this.botao_jogar.render(g);
    }
    
    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{
        
        Input input = gc.getInput();
        if (input.isKeyDown(Input.KEY_ENTER)){
            
            sbg.getState(play).init(gc, sbg);
            sbg.enterState(play, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
        }
            
        /*
        int xpos = Mouse.getX();
        int ypos = Mouse.getY();
        if ((xpos > 75 && xpos < 175) && (ypos > 160 && ypos <260)){
            if (input.isMouseButtonDown(0)){
                sbg.enterState(1);
            }
        }*/
    }
    
    public int getID(){
        return 0;
    }
}
