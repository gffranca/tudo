package objetos;

import java.util.Random;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;
import slick.Game;
import objetos.ListadeAster;

public abstract class AsteroidGeral extends Obj{
    
    protected Shape aster;
    protected Vector2f ang;
    protected static final int TAMANHO = 3;
    protected long tamanho;
    
    protected void iniciaAster(){
    
        Random random = new Random();
        this.ang = null;
        this.angulo = 0;
        if(this.tamanho == 0){
            
            this.tamanho = random.nextInt(TAMANHO - 1) +1;
            this.formato();
            this.mudaTam();
            this.spawnAster();
            float xRange = random.nextInt((Game.width) + 1);
            float yRange = random.nextInt((Game.height) + 1);
            float nX = xRange - this.x;
            float nY = yRange - this.y;
            this.ang = new Vector2f(nX, nY);
            this.angulo = (float) ang.getTheta() + 90;
        }else{
            
            this.formato();
            this.mudaTam();
        }
        
        this.velocidade = 1 * 0.5f * this.tamanho;
    }
    
    protected void mudaTam(){
        
        this.aster = this.aster.transform(Transform.createScaleTransform((float) 1/this.tamanho, (float) 1/this.tamanho));
    }
    
    protected void formato(){
        float points[][] = new float[][]{
            {120, 5, 280, 80, 305, 150, 280, 230, 150, 265, 40, 220, 5, 115},
            {225, 10, 250, 80, 300, 95, 300, 200, 215, 260, 75, 250, 5, 145, 30, 105, 50, 45},
            {65, 20, 225, 35, 195, 80, 280, 65, 315, 180, 185, 270, 160, 235, 50, 220, 15, 190, 15, 95},
            {15, 10, 130, 80, 230, 35, 300, 75, 310, 180, 250, 260, 90, 265, 70, 280, 10, 145},
            {30, 30, 190, 10, 230, 80, 290, 50, 330, 150, 290, 230, 210, 270, 155, 240, 65, 260, 10, 185, 40, 100},
            {25, 55, 170, 25, 270, 40, 315, 80, 260, 140, 315, 200, 235, 260, 155, 245, 70, 270, 10, 165, 35, 140}};/*
            {},
            {}
        };*/
        Random random = new Random();
        int r = random.nextInt(points.length);
        this.aster = new Polygon(points[r]);
    }
    
    protected void spawnAster(){

        Random random = new Random();
        float w = this.aster.getMaxX() - this.aster.getMinX();
        float h = this.aster.getMaxY() - this.aster.getMinY();
        float maior;
        if(w > h){
            
            maior = w;
        }else{
            
            maior = h;
        }
        int pos = random.nextInt(4);
        float centerX;
        float centerY;
        switch(pos){
            
            case 0: //cima
                
                centerX = random.nextInt((int) (Game.width) + 1);
                centerY = - maior;
                break;
            case 1: //dir
                
                centerX = Game.width + maior;
                centerY = random.nextInt((int) (Game.height) + 1);
                break;
            case 2: // baixo
                
                centerX = random.nextInt((int) (Game.width) + 1);
                centerY = Game.height + maior;
                break;
            default: // esq
                
                centerX = - maior;
                centerY = random.nextInt((int) (Game.height) + 1);
        }
        this.mudaPos(this.aster, centerX, centerY);
    }
    
    protected boolean fora_da_tela() {
        if ((this.x > 2 * Game.width) || (this.x < - Game.width)) return true;
        if ((this.y > 2 * Game.height) || (this.y < - Game.height)) return true;
        return false;
    }
}
