package objetos;

import java.util.Random;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

public class Asteroid extends AsteroidGeral{
    
    public Asteroid(GameContainer gc, long tam) throws SlickException{
        
        this.tamanho = tam;
        this.iniciaAster();
    }
    
    protected void render(GameContainer gc, Graphics g){
              
        if(this.tamanho == 1) this.desenha(gc, g, this.aster, 255, 0, 0);
        else if(this.tamanho == 2) this.desenha(gc, g, this.aster, 255, 153, 0);
        else this.desenha(gc, g, this.aster, 255, 255, 102);
    }
    
    protected void update(){
        
        this.move(this.aster);
    }
    
    public Shape getShape(){
    
        return this.aster;
    }
}
