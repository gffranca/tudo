package objetos;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import slick.Game;

public class Vida extends Obj{
    
    protected Shape vida;
    
    public Vida(GameContainer gc) throws SlickException{
        
        float[] points = new float[]{0, 70, 25, 70, 35, 60, 45, 70, 70, 70, 35, 0};
        this.vida = new Polygon(points);
        this.vida = this.vida.transform(Transform.createScaleTransform((float) 0.5, (float) 0.5));
        this.mudaPos(this.vida, 0, 0);
    }
    
    protected void render(GameContainer gc, Graphics g){
        
        this.desenha(gc, g, this.vida, 255, 255, 255);
    }
    
}
