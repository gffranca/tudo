package objetos;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import slick.Game;
import slick.Play;


public class Tiro extends Obj{
    protected Circle tiro;
    private int r;
    
    public Tiro(GameContainer gc, float inclinacao, float x, float y) throws SlickException{
        
        this.angulo = 0;
        this.tiro = null;
        this.r = 3;
        this.velocidade = 2.5f;
        this.angulo = inclinacao;
        this.x = (float) x;
        this.y = (float) y;
        this.tiro = new Circle(x, y, this.r);
    }
    
    protected void render(GameContainer gc, Graphics g){
        
        this.desenha(gc, g, this.tiro, 255, 255, 255);
    }
    
    protected void update(){
        
        this.move(this.tiro);
    }
    
    public boolean fora_da_tela() {
        
        if ((this.x > Game.width + this.r) || (this.x < - this.r)) return true;
        if ((this.y > Game.height + this.r) || (this.y < - this.r)) return true;
        return false;
    }
    
    public Circle getShape(){
    
        return this.tiro;
    }
}

