package objetos;

import java.util.ArrayList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;

public class ListadeTiros {
    
    protected static ArrayList<Tiro> tiros;
    
    public ListadeTiros(){
        
        tiros = new ArrayList();
    }
    
    protected void novo_tiro(GameContainer gc, float inclinacao, float x, float y) throws SlickException{
        tiros.add(new Tiro(gc, inclinacao, x, y));
    }
    
    public void update(){
        
        int j = 0;
        while( j < tiros.size()){
            tiros.get(j).update();
            if (tiros.get(j).fora_da_tela()) tiros.remove(j);
            else j++;
        }
    }
    
    public void render(GameContainer gc, Graphics g){
        
        for (int i = 0; i < tiros.size(); i++) tiros.get(i).render(gc,  g);
    }
}
