package objetos;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public abstract class Obj {
 
    protected float x;
    protected float y;
    protected float velocidade;
    protected float angulo;
    protected float delta;
    
    protected void colore(Graphics g, Shape nome, int R, int G, int B){
        
        g.setColor(new Color(R, G, B));
        g.fill(nome);
    }
    
    protected void mudaPos(Shape nome, float x, float y){
      
        nome.setCenterX(x);
        this.x = x;
        nome.setCenterY(y);
        this.y = y;
    }

    protected void desenha(GameContainer gc, Graphics g, Shape nome, int R, int G, int B){
       
        g.setColor(new Color(R, G, B));
        g.draw(nome);
    }
    
    protected void move(Shape nome){
        
        this.x += this.velocidade * (float) Math.sin(Math.toRadians(this.angulo));
        this.y -= this.velocidade * (float) Math.cos(Math.toRadians(this.angulo));
        this.mudaPos(nome, this.x, this.y);
    }
    protected void move(Circle nome){
        
        this.x += this.velocidade * (float) Math.sin(Math.toRadians(this.angulo));
        this.y -= this.velocidade * (float) Math.cos(Math.toRadians(this.angulo));
        this.mudaPos(nome, this.x, this.y);
    }
}
