package objetos;

import java.util.ArrayList;
import static objetos.ListadeTiros.tiros;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Polygon;

public class ListadeVida {
    
    protected ArrayList<Vida> vidas;
    private int nVidas;
    private float w;
    private float h;
    
    public ListadeVida(GameContainer gc) throws SlickException{
        
        vidas = new ArrayList();
        this.iniciaVidas(gc);
    }
    
    private void iniciaVidas(GameContainer gc) throws SlickException{
        nVidas = Nave.getVida();
        for(int i = 0; i < this.nVidas; i++){
            vidas.add(new Vida(gc));
            this.h = vidas.get(i).y - vidas.get(i).vida.getMinY();
            vidas.get(i).mudaPos(vidas.get(i).vida, (i + 1) * (10 + vidas.get(i).vida.getCenterX() - vidas.get(i).vida.getMinX()) + i * (vidas.get(i).vida.getMaxX() - vidas.get(i).vida.getCenterX()), 10 + this.h);
            
        }
    }
    
    protected void nova_vida(GameContainer gc) throws SlickException{
        
        vidas.add(new Vida(gc));
        if(nVidas > 1){
            
            this.h = vidas.get(nVidas - 1).y - vidas.get(nVidas - 1).vida.getMinY();
            vidas.get(nVidas - 1).mudaPos(vidas.get(nVidas - 1).vida, (nVidas) * (10 + vidas.get(nVidas - 1).vida.getCenterX() - vidas.get(nVidas - 1).vida.getMinX()) + (nVidas - 1) * (vidas.get(nVidas - 1).vida.getMaxX() - vidas.get(nVidas - 1).vida.getCenterX()), 10 + this.h);
        }else{
            
            this.h = vidas.get(0).y - vidas.get(0).vida.getMinY();
            vidas.get(0).mudaPos(vidas.get(0).vida, (10 + vidas.get(0).vida.getCenterX() - vidas.get(0).vida.getMinX()), 10 + this.h);
        }
}
    
    public void update(GameContainer gc) throws SlickException{
        
        nVidas = Nave.getVida();  
        //System.out.println(nVidas);
        if(nVidas < vidas.size()) this.perdeuVida();
        if(vidas.size() < nVidas) this.nova_vida(gc);
        //System.out.println(vidas.size());
    }
    
    public void render(GameContainer gc, Graphics g) throws SlickException{
        
        for(int i = 0; i < vidas.size(); i++){
            
            vidas.get(i).render(gc, g);
            //System.out.println("Nave " + i + ", " + vidas.get(i).x + vidas.get(i).y);
        }
    }
    
    public void perdeuVida(){
        vidas.remove(vidas.size() - 1);
    }
}