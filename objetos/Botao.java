
package objetos;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Botao {
    private int x;
    private int y;
    private String texto;
    public Botao(int x,int y, String texto){
        this.x = x;
        this.y = y;
        this.texto = texto;
    }
    public void render(Graphics g){
        g.setColor(new Color(255, 255, 255));
        g.drawRect(this.x, this.y, 100, 100);
        g.drawString(texto, this.x + 10, this.y);
        
    }
}
