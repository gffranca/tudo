package objetos;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;
import org.newdawn.slick.*;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.*;
import slick.Game;
import slick.Play;

public class Nave extends Obj{
    
    public ListadeTiros tiros; //lucas
    private long tempo_para_atirar; //lucas
    private final long delay_tiros = 201;
    protected static Shape nave;
    protected static int vida;
    private static boolean tomouDano;
    private static boolean perdeu;
    private static int time;
    
    public Nave(GameContainer gc) throws SlickException{
        
        this.angulo = 0;
        this.nave = null;
        this.tomouDano = false;
        this.perdeu = false;
        this.vida = 3;
        this.time = 0;
        this.velocidade = 0.8f;
        this.tiros = new ListadeTiros(); //lucas
        this.tempo_para_atirar = 0; //lucas
        float[] points = new float[]{0, 70, 25, 70, 35, 60, 45, 70, 70, 70, 35, 0};
        this.nave = new Polygon(points);
        this.x = gc.getWidth()/2;
        this.y = gc.getHeight()/2;
        this.mudaPos(this.nave, this.x, this.y);
    }
    
    public void render(GameContainer gc, Graphics g){
        
        this.colore(g, this.nave, 0, 0, 0);
        g.setLineWidth(2);
        if(tomouDano == true) this.desenha(gc, g, this.nave, 255, 0, 0);
        else this.desenha(gc, g, this.nave, 143, 60, 226);
        g.setLineWidth(1);
    }
    
    public void update(GameContainer gc, int i) throws SlickException{
        
        this.verificaMov(gc, i);
        if(tomouDano == true){
            
            time += i;
            if(time > 500){
                
                tomouDano = false;
                time = 0;
            }
        }
    }

    private void verificaMov(GameContainer gc, int i) throws SlickException{
        
        Input input = gc.getInput();
        if (input.isKeyDown(Input.KEY_A)){
            
            this.angulo -= this.velocidade;
            this.nave = this.nave.transform(Transform.createRotateTransform((float)(Math.toRadians(- this.velocidade)), this.nave.getCenterX(), this.nave.getCenterY()));
        }
        if (input.isKeyDown(Input.KEY_D)){
            
            this.angulo += this.velocidade;
            this.nave = this.nave.transform(Transform.createRotateTransform((float)(Math.toRadians(this.velocidade)) , this.nave.getCenterX(), this.nave.getCenterY()));
        }
        if (input.isKeyDown(Input.KEY_SPACE) && this.tempo_para_atirar > 200){
            
            this.tiros.novo_tiro(gc, this.angulo, this.x, this.y);
            this.tempo_para_atirar = 0;
        }
        if (this.tempo_para_atirar < this.delay_tiros) this.tempo_para_atirar += i;
        if (input.isKeyDown(Input.KEY_W)) this.move(this.nave);
        if (input.isKeyPressed(Input.KEY_V) && (ListadeAster.pontuacao >= 1000)){
            ganhaVida();
            ListadeAster.pontuacao -= 1000;
        }
    }
    
    private void fora_da_tela(){
        float width = this.nave.getMaxX() - this.nave.getMinX();
        float height = this.nave.getMaxY() - this.nave.getMinY();
        if (this.x > Game.width){
            
            this.x = -width;
            this.nave.setCenterX(this.x);
        }
        if (this.x + width < 0){
            
            this.x = Game.width;
            this.nave.setCenterX(this.x);
        }
        if (this.y > Game.height){
            
            this.y = -height;
            this.nave.setCenterY(this.y);
        }
        if (this.y + height < 0){
            
            this.y = Game.height;
            this.nave.setCenterY(this.y);
        }
    }
    
    @Override
    protected void move(Shape nome){
   
        this.x += this.velocidade * Math.sin(Math.toRadians(this.angulo));
        this.y -= this.velocidade * Math.cos(Math.toRadians(this.angulo));
        this.mudaPos(nome, this.x, this.y);
        this.fora_da_tela();
    }
 
    public Shape getShape(){
    
        return this.nave;
    }
    
    public static int getVida(){
        
        return vida;
    }
    
    public static void perdeVida(){
        
        if(tomouDano == false){
            
            if(vida > 0){
                
                vida--;
                tomouDano = true;
            }
            else{
                
                perdeu = true;
            }
        }
    }
    
    public static void ganhaVida(){
        
        vida++;
    }
    
    public static boolean getPerdeu(){
        
        return perdeu;
    }
    
    public void inicio(){
        
        
    }
}
